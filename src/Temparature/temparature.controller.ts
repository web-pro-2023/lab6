import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { TemparatureService } from './temparature.service';

@Controller('temparature')
export class TemparatureController {
  constructor(private readonly temparatureService: TemparatureService) {}

  @Get('convert')
  convert(@Query('celsius') celsius: string) {
    return this.temparatureService.convert(parseFloat(celsius));
  }

  @Post('convert')
  convertByPost(@Body('celsius') celsius: number) {
    return this.temparatureService.convert(celsius);
  }

  @Get('convert/:celsius')
  convertByParam(@Param('celsius') celsius: string) {
    return this.temparatureService.convert(parseFloat(celsius));
  }
}
